var refresh;

$(document).ready(function(){
	$("#rcon_button").click(function(){
		submit_rcon();
	});
	$("#rcon_command").keypress(function(e){
		if(e.which==13)
			submit_rcon();
	});
	$("#autorefresh").click(function() {
		if ($(this).val() == "false") {
			$(this).text("Auto Refresh (on)");
			$(this).val("true");
			refresh = setInterval(refresh_sections,5000);

		} else {
			$(this).text("Refresh (off)");
			$(this).val("false");
			clearInterval(refresh);
		}
	});
});

function refresh_sections() {
	$("#main_content").load('/', {"layout": "false"});
}

function submit_rcon() {
	$.post("/server/rcon",
		{
			layout: "false",
			rcon_command: $("#rcon_command").val(),
			rcon_password: $("#rcon_password").val(
		)},
		function(responseTxt,statusTxt,xhr){
			responseTxt = responseTxt.replace(/(?:\r\n|\r|\n)/g, '<br />');
			$("#rcon_console").append("> " + $("#rcon_command").val() + "<br/>");
			if(responseTxt.length==0)
				$("#rcon_console").append("Invalid command" + "<br/>");
			else
				$("#rcon_console").append(responseTxt + "<br/>");
			$("#rcon_console").scrollTop($("#rcon_console").prop('scrollHeight'));
			$("#rcon_command").val('');
		}
	);
}
