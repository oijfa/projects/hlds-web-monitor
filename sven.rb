#!/usr/bin/env ruby
# frozen_string_literal: true

require 'sinatra/base'
require 'sinatra/config_file'
require 'sinatra/multi_route'
require 'sinatra/twitter-bootstrap'
require 'tilt/erb'
require 'steam-condenser/servers/goldsrc_server'
require 'steam-condenser/servers/sockets/goldsrc_socket'
require 'steam-condenser/servers/master_server'
require 'steam-condenser/servers/sockets/master_server_socket'
require 'redis'
require './lib/helpers/partials'
require 'json'
require 'sinatra/custom_logger'
require 'logger'

set :logger, Logger.new(STDOUT)

use Rack::CommonLogger

# SvenApp base server
class SvenApp < Sinatra::Application
  register Sinatra::Twitter::Bootstrap::Assets
  register Sinatra::MultiRoute
  register Sinatra::ConfigFile
  config_file 'config.yml'
  enable :inline_templates
  enable :sessions
  set :show_exceptions, true
  set :logger, Logger.new(STDOUT)

  def initialize
    super()
  end

  # run before any request to '/' or '/server*'
  before %r{(?:^\/$|^\/server.*$)} do
    # initialize redis instance
    redis = Redis.new(host: settings.redis_address, port: settings.redis_port)

    # check if the server info needs to be updated
    if redis_cache_timeout?(redis, :hlds_last_updated, settings.server_info_refresh_delay)
      # connect to goldsrc instance
      server_connection = server_connect

      # query server stats
      server_connection.update_ping
      server_connection.update_rules
      server_connection.update_players
      server_connection.update_server_info

      # updating maps is incredibly slow with a large map list so we check a separate timeout for this
      if redis_cache_timeout?(redis, :maps_last_updated, settings.map_refresh_delay)
        redis.set(:maps, list_maps(server_connection).to_json)
      end

      # update server cache
      set_server_info(redis, server_connection)
    end

    # return server instance
    @server = get_server_info(redis)
  end

  # run before any request to /masterbrowser*
  before '/masterbrowser*' do
    @browser = masterbrowser_connect
    @browser
  end

  # perform an rcon query
  def rcon
    server_return = ''

    # connect to goldsrc instance
    server_connection = server_connect

    # attempt to auth
    server_connection.rcon_auth params[:rcon_password]
    if server_connection.rcon_authenticated?
      server_return = server_connection.rcon_exec params[:rcon_command]
      if server_return == ''
        server_return =
          server_connection.rcon_exec params[:rcon_command].scan(/\w+/)[0]
      end
    else
      server_return = 'Invalid credentials'
    end
    server_return
  end

  # check redis timeout period for service and update redis cache if out of date
  def redis_cache_timeout?(redis, redis_key, timeout)
    logger.debug '###############CHECKING REDIS ' + redis_key.to_s + ' CACHE TIMEOUT#####################'
    logger.debug 'Updated ' + (Time.now.to_i - redis.get(redis_key).to_i).to_i.to_s + ' seconds ago. Refresh rate: ' + timeout.to_s + ' seconds.'
    if ((Time.now.to_i - redis.get(redis_key).to_i).to_i) > timeout || redis.get(redis_key).nil?
      logger.debug '###############REFRESHING ' + redis_key.to_s + ' ###################'
      redis.set(redis_key, Time.now.to_i)
      redis_timeout = true
    else
      logger.debug '###############NO NEED TO UPDATE REDIS ' + redis_key.to_s + ' CACHE###################'
      redis_timeout = false
    end
    redis_timeout
  end

  # cache server status
  def set_server_info(redis, server_connection)
    redis.set(:server_name, server_connection.server_info[:server_name])
    redis.set(:game_description, server_connection.server_info[:game_description])
    redis.set(:map_name, server_connection.server_info[:map_name])
    redis.set(:players, server_connection.players.to_json)
    redis.set(:rules, server_connection.rules.to_json)
  end

  # return cached server status
  def get_server_info(redis)
    server = Hash.new
    server[:server_name] = redis.get(:server_name)
    server[:game_description] = redis.get(:game_description)
    server[:map_name] = redis.get(:map_name)
    server[:players] = JSON.parse(redis.get(:players))
    server[:rules] = JSON.parse(redis.get(:rules))
    server[:maps] = JSON.parse(redis.get(:maps))
    server
  end

  # parse map list and dispose of hlds generated markup
  def list_maps(server_connection)
    maps = server_connection.rcon_exec 'maps *'
    maps = maps.split(/\n+/)[3..-3]
    maps.sort_by(&:downcase)
  end

  # establish connection to server
  def server_connect
    server_connection = SteamCondenser::Servers::GoldSrcServer.new(settings.rcon_address, settings.rcon_port)
    unless settings.rcon_timeout.nil?
      SteamCondenser::Servers::Sockets::BaseSocket.timeout = settings.rcon_timeout
    end
    server_connection.rcon_auth settings.rcon_password
    server_connection
  end

  # establish connection to master browser
  def masterbrowser_connect
    masterbrowser_connection = SteamCondenser::Servers::MasterServer.new(*SteamCondenser::Servers::MasterServer::GOLDSRC_MASTER_SERVER)
    masterbrowser_connection
  end

  route :get, :post, ['/','/server'] do
    erb :index, layout: params[:layout] == 'false' ? false : :layout
  end

  route :get, :post, '/server/players' do
    partial 'server/players', layout: params[:layout] == 'false' ? false : :layout

  end

  route :get, :post, '/server/maps' do
    partial '/server/maps', layout: params[:layout] == 'false' ? false : :layout

  end

  route :get, :post, '/server/rules' do
    partial '/server/rules', layout: params[:layout] == 'false' ? false : :layout
  end

  get '/server/rcon_console' do
    partial '/server/rcon_console', layout: params[:layout] == 'false' ? false : :layout

  end

  post '/server/rcon' do
    rcon
  end

  get '/server/rcon' do
    partial '/server/rcon', layout: params[:layout] == 'false' ? false : :layout
  end

  #route :get, :post, ['/masterbrowser', '/masterbrowser/list'] do
  #  partial '/masterbrowser/list', layout: params[:layout] == 'false' ? false : :layout
  #end
end
