# hlds-web-monitor
A ruby/Sinatra application for monitoring and administrating Half-Life goldsrc based servers from a simple web interface. Utilized redis for caching server info to avoid excessive RCON hits.

To use first before all steps:
* Clone into the HLDS git repository
* Copy config.yml.dist to config.yml.
* Set your rcon parameters and preferred timeout time for the UI to prevent excessive queries to your hlds server instance (map lists of over 500 can take upwards of 5 seconds on a good connection)

|Note|: All following steps listed in < > are discretionary and can be named as you please.

Docker single container (expects a redis server running separately):
* edit the script for your desired port and volume name if desired)
* ./start_hlds_web_monitor.sh
* this will build an image and start a container on port 3000 (
* |Note|: if you do not have a redis server running this will fail; you can create one locally by simply running:
* *  docker run --name <redis> -d redis redis-server --appendonly yes

Docker Compose:
* edit docker-compose.yml for your desired port and volume name if desired
* docker-compose up -d
* this will use the docker-compose.yml to create an instance of the webapp and a redis server

Docker Swarm:
* edit docker-swarm.yml for your desired port, volume name, and deployment replicas if desired
* docker config create hlds_web_monitor_config ./config.yml
* docker stack deploy --compose-file docker-swarm.yml <hlds-web-monitor>
* this will use the docker-swarm.yml to create an instance of the webapp and a redis server
