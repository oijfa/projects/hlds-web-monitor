FROM ruby:alpine3.11
ENV APP_HOME /app
RUN apk add git
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
RUN git clone https://gitlab.com/oijfa/projects/hlds-web-monitor.git .
RUN gem install bundler
RUN bundle config set path 'vendor/cache'
RUN bundle install
ADD . $APP_HOME
EXPOSE 80
CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "80"]
